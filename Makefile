SHELL=/bin/bash
DOC=yet-another-oslo-cv.tex
REF=yet-another-oslo-cv.bcf 

all: 
	@pdflatex $(DOC) 
	@biber $(REF)
	@pdflatex $(DOC) 
clean:
	rm -f *.blg *.bbl *.out *.aux *.run.xml *.bcf *.log 
