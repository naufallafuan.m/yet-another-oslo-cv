# Yet Another Oslo CV

_Curriculum Vitae_ berbasiskan LaTeX 

## Dependencies 

- TeX-Live-full
- Semua paket yang ada di preamble dokumen
- Makefile 

## Cara Menggunakan 

1. Clone repository ini 
2. Buka terminal kemudian ketik 

```
cd yet-another-oslo-cv 
```
3. ngebuild CV 

```
make 
```

# Terima Kasih 🙏️
